#!/bin/bash
#--help shitty implementation
if [[ ${ARGV[0]} == "--help" || ${ARGV[0]} == "-h" || ${ARGV[0]} == "-help"|| ${ARGV[0]} == "--h" ]]
then
	echo "Help section"
	echo "=========================================================================="
	echo "This script allows to add, commit and push at the same time."
	echo "Usage: \"gitall \"commit_message\" <1st_file> <2nd_file> <3rd_file> ...\""
	echo "Note that the commit message is mandatory and it must not contain any space and must be included between \"\""
	echo "=========================================================================="
	exit 0
fi

if [ $NUM_ARGS -ge 2 ]
then
	#if NUM_ARGS>2 go on, which means there is at least a file and a commit message
	while [[ $i != $NUM_ARGS ]]; do
		#cycle through the files to add
		if [[ ! -f ${ARRAY[$i]} ]]
		#checks if the file exists
		then
			echo "warning: file <${ARRAY[$i]}> not found."
		else
			git add ${ARRAY[$i]}
			COUNT=$(($COUNT + 1))
		fi
		i=$(($i + 1))
		echo i=$i+1
	done
	if [[ COUNT -ge 1 ]]; then
		#if at least 1 file has been added
		#it does not necessarily have to be modified, since if it wasn't,
		#git won't issue an error.
		#also, it won't commit/push anything
		git commit -m ${ARRAY[0]}
		git push
		echo "Success!"
	else
		echo "Nothing to push."
	fi

else
	echo "error: number of arguments not valid"
	echo "check \"gitall --help\" "
fi
